<?php 

// You MUST modify app_tokens.php to use your own Oauth tokens
require 'app_tokens.php';

// Create an OAuth connection
require 'tmhOAuth.php';
error_reporting(0);
// Convert tweet text into linkified version
function linkify($text) {
	
	 //Linkify URLs
    $text = ereg_replace("[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]",
    	"<a target='_blank' href=\"\\0\">\\0</a>", $text); 
    	
	// Linkify @mentions
    $text = preg_replace("/\B@(\w+(?!\/))\b/i", 
    	'<a target="_blank" href="https://twitter.com/\\1">@\\1</a>', $text); 
    	
	// Linkify #hashtags
    $text = preg_replace("/\B(?<![=\/])#([\w]+[a-z]+([0-9]+)?)/i", 
    	'<a target="_blank" href="https://twitter.com/#!/search/%23\\1">#\\1</a>', $text); 
    	
    return $text;
}

// Convert a tweet creation date into Twitter format
function twitter_time($time) {

  // Get the number of seconds elapsed since this date
  $delta = time() - strtotime($time);
  if ($delta < 60) {
    return 'less than a minute ago';
  } else if ($delta < 120) {
    return 'about a minute ago';
  } else if ($delta < (60 * 60)) {
    return floor($delta / 60) . ' minutes ago';
  } else if ($delta < (120 * 60)) {
    return 'about an hour ago';
  } else if ($delta < (24 * 60 * 60)) {
    return floor($delta / 3600) . ' hours ago';
  } else if ($delta < (48 * 60 * 60)) {
    return '1 day ago';
  } else {
    return number_format(floor($delta / 86400)) . ' days ago';
  } 
}

		

$connection = new tmhOAuth(array(
  'consumer_key'    => $consumer_key,
  'consumer_secret' => $consumer_secret,
  'user_token'      => $user_token,
  'user_secret'     => $user_secret
));

// Get the timeline with the Twitter API
//$http_code = $connection->request('GET',$connection->url('1.1/statuses/user_timeline.json'), 
//	array( 'screen_name' => 'kenring', 'count' => 10));

		$http_code = $connection->request('GET',$connection->url('1.1/search/tweets.json'), 
		array('q' => '#modi', 'count' => 15));
		
// Request was successful
if ($http_code == 200) {
	
	// Extract the tweets from the API response
	$tweet_data = json_decode($connection->response['response'],true);
	//https://twitter.com/Disaster_Update/status/347257549046808576
	// Accumulate tweets from results
	$tweet_stream = array();	 
	$i=0;
	 
	//var_dump($tweet_data['statuses'][1]);
	foreach($tweet_data['statuses'] as $tweet) {
		
		if($tweet_data['statuses'][$i]['user']['location']=="India"){
			$city = $tweet_data['statuses'][$i]['user']['time_zone'];
			$name = $tweet_data['statuses'][$i]['user']['screen_name'];
			$twitter = $tweet_data['statuses'][$i]['text'];
			$tweet_stream[$city][] = array("name" => $name,  "twitter"=> $twitter );
		
		// Add this tweet's text to the results
	
		$tweet_stream .= '<div style="display:block;"><a href="http://twitter.com/'.$tweet['statuses'][$i]['user']['screen_name'].'/status/'.$tweet['id_str'].'" target="_blank">'.twitter_time($tweet['created_at']).'</a> <span class="tweet_text">'.linkify($tweet['text']).'</span></div>'.$tweet['point']['coordinates']; 
		}
		$i++;
	}
		
	// Send the tweets back to the Ajax request
	print $tweet_stream;
	
// Handle errors from API request
} else {
	if ($http_code == 429) {
		print 'Error: Twitter API rate limit reached';
	} else {
		print 'Error: Twitter was not able to process that request';
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Marker</title>
<style>
#map_div{
	width:100%;
	height:500px;	
}
</style>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script type="text/javascript" src="js/gmap3.js"></script>
<script type="text/javascript" src="http://www.bdcc.co.uk/LatLngToOSGB.js"></script>
</head>

<body>
<script>

$(document).ready(function() {

		viewportheight = ($(window).height());

		viewportwidth = ($(window).width());

		var viewable_viewportheight = parseInt(viewportheight);

		document.getElementById('map_div').style.height = '340px';




		$('#map_div').gmap3(

		  { action:'init',

			options:{

			  center:[51.404707,-0.257032],

			  zoom: 10,
			  scrollwheel: false

			}

		  },

		  { action: 'addMarkers',

			markers:[ {lat:51.404707, lng:-0.257032, data:'<b>Saffron Square</b>',options:{icon:'images/house_map.png'}}],

			marker:{

			  options:{

				draggable: false

			  },

			  events:{

				mouseover: function(marker, event, data){

				  var map = $(this).gmap3('get'),

					  infowindow = $(this).gmap3({action:'get', name:'infowindow'});

				  if (infowindow){

					infowindow.open(map, marker);

					infowindow.setContent(data);

				  } else {

					$(this).gmap3({action:'addinfowindow', anchor:marker, options:{content: data}});

				  }

				},

				mouseout: function(){

				  var infowindow = $(this).gmap3({action:'get', name:'infowindow'});

				  if (infowindow){

					infowindow.close();

				  }

				}

			  }

			}

		  }

		);

		

		});

</script>

    	<div class="small_banne_holder" id="map_div">
        	<!--<img src="images/map_banner.png" />-->
        </div>
  

</body>
</html>