<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <title>TimeSets demo</title>
        
        <link href="css/cupertino/jquery-ui.min.css" type="text/css" rel="stylesheet" />
        <link href="css/core.css" type="text/css" rel="stylesheet" />
        <link href="css/timeSets.css" type="text/css" rel="stylesheet" />
        <link href="css/articleViewer.css" type="text/css" rel="stylesheet" />
        <link href="css/legend.css" type="text/css" rel="stylesheet" />

    </head>
    
    <body width=100% height=100% oncontextmenu="return false;" style="overflow:hidden">
    	<?php
		$content = file_get_contents('capture.json');
		$obj = json_decode($content);
		
		$arr = $obj->search;
		?>
        <table width="1000" border="0" cellspacing="0" cellpadding="5" align="center">
      <tr>
        <td><strong>Time</strong></td>
        <td><strong>Zoom</strong></td>
        <td><strong>Open topics</strong></td>
      </tr>
      <?php
	  foreach($arr as $r){
		  ?>
      <tr>
        <td><?php echo $r->time;?></td>
        <td><?php echo $r->zoom;?></td>
        <td>
		<?php $content = $r->content;
		$arr_c = explode('~',$content);
		echo '<ul>';
		foreach($arr_c as $c){
			if($c!=""){
				echo '<li>'.$c.'</li>';
			}
		}
		echo '</ul>';
		?></td>
      </tr>
      <?php
	  }
	  ?>
    </table>

    </body>
</html>