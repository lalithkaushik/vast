<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <title>TimeSets demo</title>
        
        <link href="css/cupertino/jquery-ui.min.css" type="text/css" rel="stylesheet" />
        <link href="css/core.css" type="text/css" rel="stylesheet" />
        <link href="css/timeSets.css" type="text/css" rel="stylesheet" />
        <link href="css/articleViewer.css" type="text/css" rel="stylesheet" />
        <link href="css/legend.css" type="text/css" rel="stylesheet" />

        <script type="text/javascript" src="lib/d3.js"></script>
        <script type="text/javascript" src="lib/jquery-2.1.0.min.js"></script>
        <script type="text/javascript" src="lib/jquery-ui.min.js"></script>
        <script type="text/javascript" src="lib/colorbrewer.js"></script>
        
        <script type="text/javascript" src="js/timeSetsDemo.js"></script>

        <script type="text/javascript" src="js/core.js"></script>    
        <script type="text/javascript" src="js/vises/timeSets.js"></script>
        <script type="text/javascript" src="js/widgets/articleViewer.js"></script>
        <script type="text/javascript" src="js/widgets/legend.js"></script>
        <script type="text/javascript" src="js/algorithms/shapeOutline.js"></script>
    </head>
    
    <body width=100% height=100% oncontextmenu="return false;" style="overflow:hidden">
    	<div align="center" style="position:absolute; text-align:center; padding:10px; width:100%;">
        <input name="button" type="button" value="Save" onClick="export_file()"> &nbsp; <input name="button" type="button" value="View" onClick="window.location='view.php'"></div>
    </body>
</html>