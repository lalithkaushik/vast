/**
 * Declares the core object "sm" of the framework.
 * Includes helper functions.
 */
var sm = function() {
    var sm = {
        "version": "0.1",
        "id": 0
    };

    sm.TRANSITION_SHOW_HIDE = 250;
    sm.TRANSITION_MOVEMENT = 750;
    
    /**
	 * Checks whether the DOM element contains a given point.
	 */
	Element.prototype.containsPoint = function(pos) {
		var rect = this.getBoundingClientRect();
	    return pos.x >= rect.left && pos.x <= rect.right && pos.y >= rect.top && pos.y <= rect.bottom;
	};

	/**
	 * Returns the center of the DOM element.
	 */
	Element.prototype.getCenterPoint = function() {
        var rect = this.getBoundingClientRect();
        return rect ? { "x": rect.left + rect.width / 2, "y": rect.top + rect.height / 2 } : null;
    }

    /**
	 * Moves the DOM element to front.
	 */
	Element.prototype.moveToFront = function() { 
	    this.parentNode.appendChild(this); 
	};

    /**
	 * Moves the selection to front.
	 */
	d3.selection.prototype.moveToFront = function() { 
	    return this.each(function() { 
	        this.moveToFront();
	    }); 
	};

	/**
	 * Returns unique id.
	 */
	sm.getUniqueId = function() {
	    return sm.id++;
	};

	/**
	 * Creates a 1D array with the given initial value. 
	 */
	sm.createArray = function(length, initialValue) {
	    return d3.range(length).map(function() { return initialValue; });
	};

	/**
	 * Creates a 2D array with the given initial value. 
	 */
	sm.createArray2D = function(length1, length2, initialValue) {
	    return d3.range(length1).map(function() { return sm.createArray(length2, initialValue); });
	};

	return sm;
}();