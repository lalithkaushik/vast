document.addEventListener('DOMContentLoaded', function () {
    fetchHistory();

    function fetchHistory() {
        var maxResults = 1000;
        var endTime = new Date();
        var numDays = 7;
        var starTime = endTime - numDays * 24 * 60 * 60 * 1000;

        chrome.history.search({
            'text': '',
            'startTime': starTime,
            'endTime': +endTime,
            'maxResults': maxResults
        }, function(historyItems) {
            console.log("Items = " + historyItems.length);
            addTimeline(historyItems);
            // addList(historyItems);
        });
    };

    /**
     * Displays history as a list.
     * https://developer.chrome.com/extensions/history#type-HistoryItem
     */
    function addList(historyItems) {
        var list = $("<ol></ol>").appendTo("body");
        var format = d3.time.format("%H:%M");

        historyItems.forEach(function(item) {
            var listItem = $("<li class='entry'></li>").appendTo(list);
            var container = $("<div class='entry-container'></div>").appendTo(listItem);

            container.append("<div class='time'>" + format(new Date(item.lastVisitTime)) + "</div>");            
            container.append("<img class='icon' src='" + ("chrome://favicon/size/16@1x/" + item.url) + "'/img>");
            container.append("<div class='title'>" + (item.title || item.url) + "</div>");
        });
    }

    /**
     * Display history using TimeSets.
     */
    function addTimeline(historyItems) {
        // Prepare data
        var events = [];
        var links = [];
        var themesDict = {};
        var data = { "events": events, "links": links };
        var visitIdToIndex = {};
        var count = 0;
        var totalItems = historyItems.length;
        var numOutstandingRequests = 0;

        historyItems.forEach(function(item) {
            // Ignore redirect URLs from Google search
            if (item.url.match(/^https:\/\/www.google.com\/url?/)) {
                totalItems--;
                return;
            }

            // Add a new event for each visit. Use its domain as theme.
            var event = { "time": new Date(item.lastVisitTime), "title": item.title, "url": item.url };
            if (!event.title) {
                numOutstandingRequests++;
                $.ajax({
                    url: event.url
                }).done(function(html) {
                    try {
                        var re = /<title>(.*)<\/title>/;
                        var result = html.match(re);
                        event.title = result ? result[1] : event.url;
                    } catch (e) {
                        console.log(e);
                        event.title = event.url;
                    }
                    
                    numOutstandingRequests--;
                    if (count === totalItems && !numOutstandingRequests) {
                        buildTimeline(data, visitIdToIndex, themesDict);
                    }
                }).fail(function(e) {
                    console.log(e);
                    event.title = event.url;

                    numOutstandingRequests--;
                    if (count === totalItems && !numOutstandingRequests) {
                        buildTimeline(data, visitIdToIndex, themesDict);
                    }
                });
            }

            event.content = "visitCount = " + item.visitCount;
            event.content += "<br/>typedCount = " + item.typedCount;

            // Get the corresponding visitItem by comparing visit time
            chrome.history.getVisits({ url: item.url }, function(visitItems) {
                // https://developer.chrome.com/extensions/history#type-VisitItem
                // Find the referrer
                event.content += "<br/>";

                for (var i = 0; i < visitItems.length; i++) {
                    var visitItem = visitItems[i];
                    if (visitItem.visitTime === item.lastVisitTime) {
                        visitIdToIndex[visitItem.visitId] = count;
                        event.content += "<br/>" + visitItem.referringVisitId + "  " + visitItem.transition + "  " + visitItem.visitId;

                        if (visitItem.referringVisitId !== "0") {
                            // links.push({ "source": visitItem.referringVisitId, "target": count });
                        }
                        
                        break;
                    }
                }

                count++;
                
                if (count === totalItems && !numOutstandingRequests) {
                    buildTimeline(data, visitIdToIndex, themesDict);
                }
            }); 

            var r = /:\/\/(.[^/]+)/;
            var domain = (item.url.match(r))[1];
            event.themes = [domain];

            events.push(event);

            if (!themesDict[domain]) {
                themesDict[domain] = 0;
            }

            themesDict[domain]++;
        });
    }

    function buildTimeline(data, visitIdToIndex, themesDict) {
        console.log("After removing Google Search redirecton = " + data.events.length);

        // Update source component of links when lookup is available
        data.links.forEach(function(d) {
            d.source = visitIdToIndex[d.source];
        });
        data.links = data.links.filter(function(d) { return d.source; });

        // Only use top 8 domains
        var themeCount = d3.entries(themesDict);
        themeCount.sort(function(a, b) { return d3.descending(a.value, b.value) });
        themeCount.splice(8, themeCount.length);
        data.themes = themeCount.map(function(d) { return d.key; });
        data.events = data.events.filter(function(d) { return data.themes.indexOf(d.themes[0]) !== -1; });

        // Build vis container
        var h = $(window).height() - 4;
        var svg = d3.select("body").append("svg")
            .attr("width", "100%")
            .attr("height", h);
            
        var w = $(window).width();
        var vis = svg.append("g");
        h += 3;
        vis.attr("transform", "translate(0, " + h + ")");

        // Add timeline to the visulisation
        h -= 30;
        var timeSets = sm.timeSets()
            .width(w)
            .height(h)
            .applyLayout(false)
            .setMode("background")
            .margin({ top: 20, right: 290, bottom: 20, left: 20 })
            .eventViewer(function(selection) { // Open web page of the history item
                selection.each(function(d) {
                    window.open(d.url, '_blank');
                });
            });
        vis.datum(data).call(timeSets);
    }
});