document.addEventListener('DOMContentLoaded', function () {
    // Prevent context-menu. Right-click for drawing links.
    document.addEventListener("contextmenu", function(e) {
        e.preventDefault();
    });

    loadFindings();

    // Respond message
    chrome.runtime.onMessage.addListener(function(request) {
        if (request.name === "findingUpdated") { // A finding is just updated in content script
            loadFindings();
        }
    });

    var ffa, svg, container, data = [];

    /**
     * Load all existing findings to the space.
     */
    function loadFindings() {
        chrome.storage.sync.get("findings", function(item) {
            // Parse item.findings to an array of findings
            if (item.findings) { 
                for (var webUrl in item.findings) {
                    item.findings[webUrl].map(function(d) {
                        var key = webUrl + "###" + d.path;
                        if (data.indexOf(key) === -1) {
                            data.push({ "key": key, "value": d.note });
                        }
                    });
                }

                // Initialize container and the visualization
                var mainView = getView(chrome.extension.getURL("index.html"));
                
                if (!ffa) {
                    svg = d3.select(mainView.document.body).append("svg");
                    var w = $(mainView).width();
                    var h = $(mainView).height() - 8; // Why full height so scroolbar?
                    svg.attr("width", w).attr("height", h);
                    container = svg.append("g");
        
                    ffa = sm.ffa()
                        .width(w)
                        .height(h);
                }

                container.datum(data).call(ffa);
            }
        });
    }

    function getView(url) {
        var views = chrome.extension.getViews();
        for (var i = 0; i < views.length; i++) {
            var view = views[i];
            if (view.location.href === url) {
                return view;
            }
        }

        return null;
    }
});