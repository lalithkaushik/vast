/**
 * FFA module provides supports for free-form argumentation.
 */
sm.ffa = function() {
    var width = 400,
        height = 250
        itemWidth = 200; 

    // Scale
    var xScale = d3.scale.linear()
        .domain([0, 1]);
    var yScale = d3.scale.linear()
        .domain([0, 1]);
    var scale = 1;
    var translate = [0, 0];

    var container, // g element containing the entire visualization
        nodeContainer, // g element containing all nodes
        linkContainer, // g element containing all links
        cursorLink; // the link between dragging item and current mouse cursor

    // Key function to bind data
    var key = function(d) {
        return d.key;
    };

    var dispatch = d3.dispatch("itemClicked");
    
    /**
     * Main entry of the module.
     */
    function module(selection) {
        selection.each(function(data) {
            // Update scale
            xScale.range([0, width * 0.8]); // 0.8: quick and dirty way to make the notes stay more inside the space
            yScale.range([0, height * 0.8]);

            // Add elements
            if (!container) {
                // Add invisible rectangle covering the entire space to listen to mousewheel event.
                // Otherwise, only zoom when mouse-overing visible items.
                var parent = d3.select(this);
                parent.append("rect")
                    .attr("width", width)
                    .attr("height", height)
                    .style("fill", "none")
                    .style("pointer-events", "all");
                parent.call(registerZoom);

                container = parent.append("g");
                linkContainer = container.append("g").attr("class", "sm-notes-links");
                nodeContainer = container.append("g");
                cursorLink = container.append("line").attr("class", "sm-notes-cursor-link");
            }

            // Data join
            var items = nodeContainer.selectAll("g").data(data, key);

            // Enter
            var g = items.enter().append("g")
                .attr("transform", function(d) { 
                    return "translate(" + Math.floor(xScale(Math.random())) + "," + Math.floor(yScale(Math.random())) + ")" ;
                }).on("click", function() {
                    dispatch.itemClicked(this);
                }).call(registerDrag);
            
            // foreignobject > div
            var fo = g.append("foreignObject")
                .attr("width", "100%")
                .attr("height", "100%");
            var itemContainer = fo.append("xhtml:div")
                .classed("sm-notes-node", true)        
                .text(function(d) {
                    return d.value;
                });

            // Assign node position to its data
            g.call(assignNodePosition);

            // Exit
            items.exit().transition().attr("opacity", 0).remove();
        });
    }

    function assignNodePosition(items) {
        items.datum(function(d) {
            var p = this.querySelector(".sm-notes-node").getCenterPoint();

            // Translate from screen to world coordinates
            d.x = (p.x - translate[0]) / scale;
            d.y = (p.y - translate[1]) / scale;

            return d;
        });
    }

    /**
     * Adds zoom interaction for 'parent'.
     */
    function registerZoom(parent) {
        // Apply zoom to parent element (parent), but translate the child group
        parent.call(d3.behavior.zoom()
            .on("zoom", function() {
                container.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
                scale = d3.event.scale;
                translate = d3.event.translate;
            }));
    }

    /**
     * Adds drag-n-drop interaction for 'items', each should be a g element.
     */
    function registerDrag(items) {
        items.call(d3.behavior.drag()
            .on("dragstart", function() {
                // When item is dragged, cancel following listeners, which can conflict with dragging such as zooming
                d3.event.sourceEvent.stopPropagation();
            }).on("drag", function(d) {
                // If right mouse, draw links
                if (d3.event.sourceEvent && d3.event.sourceEvent.which === 3) {
                    updateCursorLink(d, this, true);
                } else { // Otherwise, drag it
                    // Get current transform
                    var t = d3.transform(d3.select(this).attr("transform")).translate;
                    
                    // Update it with new offset
                    t[0] += d3.event.dx;
                    t[1] += d3.event.dy;
                    d3.select(this).attr("transform", "translate(" + t[0] + "," + t[1] + ")");
                   
                    // Move the dragging item atop 
                    this.moveToFront();

                    // Update position data
                    d.x += d3.event.dx;
                    d.y += d3.event.dy;

                    // Update links
                    linkContainer.selectAll("line").call(updateLinkPosition);
                }
            }).on("dragend", function(d) {
                if (d3.event.sourceEvent && d3.event.sourceEvent.which === 3) {
                    updateCursorLink(d, this, false);                    
                }
            }));
    }

    function updateLinkPosition(links) {
        links.each(function(d) {
            d3.select(this)
                .attr("x1", d.source.x).attr("y1", d.source.y)
                .attr("x2", d.target.x).attr("y2", d.target.y);
        });
    }

    /**
     * Updates location of the link.
     */
    function updateCursorLink(d, node, show) {
        cursorLink.style("display", show ? "block" : "none");
        var div = d3.select(node).select(".sm-notes-node").node();

        if (show) {
            // Link from center of div box to current mouse position
            cursorLink
                .attr("x1", d.x).attr("y1", d.y)
                .attr("x2", d3.event.x).attr("y2", d3.event.y);

            cursorLink.moveToFront();
            div.classList.add("sm-notes-node-hover");
        } else {
            // If drag ends at some item, draw link to it
            container.selectAll(".sm-notes-node").each(function(d2) {
                if (d2 !== d && this.containsPoint(d3.event.sourceEvent)) {
                    // Draw link
                    var targetCenterPoint = this.getCenterPoint();
                    linkContainer.append("line")
                        .attr("x1", d.x).attr("y1", d.y)
                        .attr("x2", d2.x).attr("y2", d2.y)
                        .datum({ source: d, target: d2 });
                }
            });

            div.classList.remove("sm-notes-node-hover");
        }
    }

    /**
     * Sets/gets the width of the control. 
     */
    module.width = function(value) {
        if (!arguments.length) return width;
        width = value;
        return this;
    };

    /**
     * Sets/gets the height of the control. 
     */
    module.height = function(value) {
        if (!arguments.length) return height;
        height = value;
        return this;
    };
    
    // Binds custom events
    d3.rebind(module, dispatch, "on");

    return module;
};