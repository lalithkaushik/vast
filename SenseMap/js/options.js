document.addEventListener('DOMContentLoaded', function () {
    // Apply saved settings
    chrome.storage.sync.get("automaticCapture", function(item) {
        document.getElementById("ckbAutomaticCapture").checked = item.automaticCapture;
    });

    // Register listener
    document.getElementById("ckbAutomaticCapture").addEventListener('click', function() {
        chrome.storage.sync.set({ "automaticCapture": this.checked });
    });

	document.getElementById("btnClearStorage").addEventListener('click', function() {
		// It seems that chrome storage limit 20 items! Must clear it at some point.
        chrome.storage.sync.set({ "findings": null });
    });    
});