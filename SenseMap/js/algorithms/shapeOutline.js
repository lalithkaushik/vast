/**
 * SHAPE_OUTLINE module provides boundary for a set of rectangles.
 */
sm.shapeOutline = function() {
    var rects, // Input set of rectangles { left, right, top, bottom, level, part } in integer, part is top/middle/bottom
        extremeRects, // Rectangles of the boundary in each level
        polygonalBoundary, // The polygonal boundary of all rectangles
        verticalMode = "rounded", // Option to draw a 'vertical' line for bends: rounded/ellipse/curves
        fillHorizontalGap = false;

    function module() {
        // Assign rectangles to levels
        var levelRects = [];
        rects.forEach(function(d) {
            var level = d.level;
            if (!levelRects[level]) {
                levelRects[level] = [];
            }
            
            levelRects[level].push(d);
        });
        
        // In each level, sort increasingly by left.
        levelRects.forEach(function(d) {
            if (d) {
                d.sort(function(a, b) {
                    return d3.ascending(a.left, b.left);
                });
            }
        });
        
        // Merge all rectangles in the same level to remove holes and gaps
        extremeRects = [];
        levelRects.forEach(function(d, i) {
            if (d) {
                var first = d[0];
                var last = d[d.length - 1];
                var left = first.left;
                var right = last.right;
                
                // Fill h-gap between layers
                if (fillHorizontalGap) {
                    if (i < levelRects.length - 1) {
                        var nextLevelRects = levelRects[i + 1];
                        if (nextLevelRects) {
                            var nextFirst = nextLevelRects[0];
                            var nextLast = nextLevelRects[nextLevelRects.length - 1];
                            var threshold = 40; // Narrower than this threshold means gap
                            if (last.right - nextFirst.left < threshold) { // Extend
                                right = nextFirst.left + threshold;
                            } else if (nextLast.right - first.left < threshold) { // Extend
                                left = nextLast.right - threshold;
                            }
                        }
                    }
                }
                 
                extremeRects[i] = { 
                    bottomLeft: { x: left, y: first.bottom },
                    topLeft: { x: left, y: first.top },
                    topRight: { x: right, y: last.top },
                    bottomRight: { x: right, y: last.bottom },
                    part: d[0].part
                };
            }
        });

        // Extend top and bottom part for better perception of set intersection
        // - Find the range of top and bottom parts
        var startBottom = stopBottom = -1;
        var startTop = stopTop = -1;
        extremeRects.forEach(function(d, i) {
            if (d) {
                if (d.part === "bottom" && startBottom == -1) {
                    startBottom = i;
                }
                if ((d.part === "middle" || d.part === "top") && stopBottom == -1) {
                    stopBottom = i;
                }
                if (d.part === "top" && startTop == -1) {
                    startTop = i;
                }
            }
        });

        if (startBottom !== -1 && stopBottom == -1) {
            stopBottom = extremeRects.length;
        }
        if (startTop !== -1) {
            stopTop = extremeRects.length;
        }

        var padding = 20;

        // - Extend bottom as inverse pyramid
        var bottomMinX = Number.MAX_VALUE, bottomMaxX = -Number.MAX_VALUE;
        if (startBottom !== -1) {
            var leftMostIndex = -1, leftMostValue = Number.MAX_VALUE;
            var rightMostIndex = -1, rightMostValue = -Number.MAX_VALUE;
            for (var i = startBottom; i < stopBottom; i++) {
                var r = extremeRects[i];
                if (r.bottomLeft.x < leftMostValue) {
                    leftMostValue = r.bottomLeft.x;
                    leftMostIndex = i;
                }
                if (r.bottomRight.x > rightMostValue) {
                    rightMostValue = r.bottomRight.x;
                    rightMostIndex = i;
                }
            }

            var prev;
            for (var i = startBottom; i < stopBottom; i++) {
                var r = extremeRects[i];
                if (i === stopBottom - 1 && extremeRects[stopBottom]) {
                    // Add extra padding row to see intersection easier, if need
                    var r2 = extremeRects[stopBottom];
                    r2.bottomLeft.x = r2.topLeft.x = Math.min(r2.bottomLeft.x, r.bottomLeft.x);
                    r2.bottomRight.x = r2.topRight.x = Math.max(r2.bottomRight.x, r.bottomRight.x);
                }

                var currentLeft = r.bottomLeft.x;
                var currentRight = r.bottomRight.x;

                if (i <= leftMostIndex + 1) {
                    bottomMinX = Math.min(bottomMinX, r.bottomLeft.x);
                    r.bottomLeft.x = r.topLeft.x = bottomMinX;
                } else {
                    r.bottomLeft.x = r.topLeft.x = Math.min(r.bottomLeft.x - padding, prev.left);
                }
                if (i <= rightMostIndex + 1) {
                    bottomMaxX = Math.max(bottomMaxX, r.bottomRight.x);
                    r.bottomRight.x = r.topRight.x = bottomMaxX;
                } else {
                    r.bottomRight.x = r.topRight.x = Math.max(r.bottomRight.x + padding, prev.right);
                }

                prev = { "left": currentLeft, "right": currentRight };
            }
        }

        // - Extend top as pyramid
        var topMinX = Number.MAX_VALUE, topMaxX = -Number.MAX_VALUE;
        if (startTop !== -1) {
            var leftMostIndex = -1, leftMostValue = Number.MAX_VALUE;
            var rightMostIndex = -1, rightMostValue = -Number.MAX_VALUE;
            for (var i = stopTop - 1; i >= startTop; i--) {
                var r = extremeRects[i];
                if (r.bottomLeft.x < leftMostValue) {
                    leftMostValue = r.bottomLeft.x;
                    leftMostIndex = i;
                }
                if (r.bottomRight.x > rightMostValue) {
                    rightMostValue = r.bottomRight.x;
                    rightMostIndex = i;
                }
            }

            var prev;
            for (var i = stopTop - 1; i >= startTop; i--) { 
                var r = extremeRects[i];
                if (i === startTop && extremeRects[startTop - 1]) {
                    // Add extra padding row to see intersection easier
                    var r2 = extremeRects[startTop - 1];
                    r2.bottomLeft.x = r2.topLeft.x = Math.min(r2.bottomLeft.x, r.bottomLeft.x);
                    r2.bottomRight.x = r2.topRight.x = Math.max(r2.bottomRight.x, r.bottomRight.x);
                }

                var currentLeft = r.bottomLeft.x;
                var currentRight = r.bottomRight.x;

                // After the left-most row, just add padding and cover previous text
                if (i >= leftMostIndex - 1) {
                    topMinX = Math.min(topMinX, r.bottomLeft.x);
                    r.bottomLeft.x = r.topLeft.x = topMinX;
                } else {
                    r.bottomLeft.x = r.topLeft.x = Math.min(r.bottomLeft.x - padding, prev.left);
                }
                if (i >= rightMostIndex - 1) {
                    topMaxX = Math.max(topMaxX, r.bottomRight.x);
                    r.bottomRight.x = r.topRight.x = topMaxX;
                } else {
                    r.bottomRight.x = r.topRight.x = Math.max(r.bottomRight.x + padding, prev.right);
                }
                
                prev = { "left": currentLeft, "right": currentRight };
            }
        }
        
        // Find polygonal boundary
        // - Left-side
        polygonalBoundary = [];
        extremeRects.forEach(function(d) {
            if (!d) {
                return true;
            }
            
            polygonalBoundary.push(d.bottomLeft);
            polygonalBoundary.push(d.topLeft);
        });
    
        // - Right-side
        extremeRects.reverse();
        extremeRects.forEach(function(d) {
            if (!d) {
                return true;
            }
            
            polygonalBoundary.push(d.topRight);
            polygonalBoundary.push(d.bottomRight);
        });

        extremeRects.reverse();
    }
    
    /**
     * Sets the input set of rectangles. 
     */
    module.rects = function(value) {
        rects = value;
        return this;
    };

    /**
     * Sets the fill horizontal gap option.
     */
    module.fillHorizontalGap = function(value) {
        fillHorizontalGap = value;
        return this;
    };

    /**
     * Returns the extreme rectangular boundary. 
     */
    module.getExtremeRects = function() {
        return extremeRects.map(function(d) { return { left: d.topLeft.x, right: d.topRight.x }; });
    };

    /**
     * Generates the 45 degree path for polygonal boundary.
     */
    module.generate45Path = function(r) {
        if (polygonalBoundary.length < 2) {
            return null;
        }

        smoothBoundary(r * 4);

        // Directions for each horizontal segment: left/right
        var dirs = [];
        for (i = -1; i < polygonalBoundary.length - 1; i += 2) {
            var px = polygonalBoundary[(i + polygonalBoundary.length) % polygonalBoundary.length].x;
            var cx = polygonalBoundary[i + 1].x;
            dirs.push(px > cx ? "left" : "right");
        }

        // Find start point
        var startPoint = { x: polygonalBoundary[0].x, y: polygonalBoundary[0].y };
        var d = Math.abs(polygonalBoundary[1].y - polygonalBoundary[0].y) / 2;
        startPoint.x += dirs[0] === dirs[1] ? d : (verticalMode === "curves" ? d / 2 : r);

        var path = "M" + startPoint.x + "," + startPoint.y + " ";
        
        // Generate
        var numSegments = polygonalBoundary.length / 2;
        for (var i = 0; i < numSegments; i++) {
            // Vertical curve
            path += generateVerticalCurve(polygonalBoundary[i * 2], polygonalBoundary[i * 2 + 1], 
                polygonalBoundary[(i * 2 - 1 + polygonalBoundary.length) % polygonalBoundary.length], 
                polygonalBoundary[(i * 2 + 2) % polygonalBoundary.length],
                dirs[i], dirs[(i + 1) % numSegments], r);

            // Horizontal curve
            path += generateHorizontalCurve(polygonalBoundary[(i * 2 + 2) % polygonalBoundary.length], 
                polygonalBoundary[(i * 2 + 3) % polygonalBoundary.length], 
                polygonalBoundary[(i * 2 + 1) % polygonalBoundary.length], 
                polygonalBoundary[(i * 2 + 4) % polygonalBoundary.length], 
                dirs[(i + 1) % numSegments], dirs[(i + 2) % numSegments], r);
        }
        
        return path;
    }

    /**
     * Generates a 'vertical' curve to replace the vertical straight segment
     */
    function generateVerticalCurve(currentPoint, nextPoint, pPoint, nnPoint, currentDir, nextDir, r) {
        if (currentDir === nextDir) { // 45, a Bezier curve with currentPoint and nextPoint are 2 control points
            var d = Math.min(Math.abs(nextPoint.y - currentPoint.y), Math.abs(currentPoint.x - pPoint.x), Math.abs(nextPoint.x - nnPoint.x)) / 2;
            var target = { "x": currentPoint.x + (nextDir === "left" ? -d : d), "y": nextPoint.y };
            return "C" + currentPoint.x + "," + currentPoint.y + " " + nextPoint.x + "," + nextPoint.y + " " + target.x + "," + target.y + " ";
        } else { // 90
            var curveString;

            if (verticalMode === "rounded") {
                // - Rounded 1
                curveString = "a" + r + "," + r + " 0 0 ";
                var x = currentDir === "left" ? -r : r;
                var y = nextPoint.y > currentPoint.y ? r : -r;
                var sweepFlag = currentDir === "left" ? "1" : "0";
                if (nextPoint.y > currentPoint.y) {
                    sweepFlag = currentDir === "left" ? "0" : "1";
                }
                curveString += sweepFlag + " " + x + "," + y + " ";

                // - Vertical line
                var h = Math.abs(nextPoint.y - currentPoint.y) - r * 2;
                curveString += "v" + (nextPoint.y > currentPoint.y ? h : -h);

                // - Rounded 2
                curveString += " a" + r + "," + r + " 0 0 ";
                x = nextDir === "right" ? r : -r;
                curveString += sweepFlag + " " + x + "," + y + " ";
            } else if (verticalMode === "ellipse") {
                var x = 0;
                var y = nextPoint.y - currentPoint.y;
                // r = Math.abs(nextPoint.y - currentPoint.y) / 35 * r;
                r = Math.log(Math.abs(nextPoint.y - currentPoint.y) / 12) * r;
                var curveString = "a" + r + "," + (y / 2) + " 0 0 ";
                var sweepFlag = currentDir === "left" ? "1" : "0";
                if (nextPoint.y > currentPoint.y) {
                    sweepFlag = currentDir === "left" ? "0" : "1";
                }
                curveString += sweepFlag + " " + x + "," + y + " ";
            } else if (verticalMode === "curves") {
                // - Curve 1
                var d = Math.min(Math.abs(nextPoint.y - currentPoint.y), Math.abs(currentPoint.x - pPoint.x), Math.abs(nextPoint.x - nnPoint.x)) / 4;
                var midPointY = currentPoint.y + (nextPoint.y - currentPoint.y) / 2;
                var target = { "x": currentPoint.x + (currentDir === "left" ? -d : d), "y": midPointY };
                var curveString = "C" + currentPoint.x + "," + currentPoint.y + " " + nextPoint.x + "," + midPointY + " " + target.x + "," + target.y + " ";

                // - Curve 2
                target = { "x": currentPoint.x + (nextDir === "left" ? -d : d), "y": nextPoint.y };
                curveString += " C" + nextPoint.x + "," + midPointY + " " + nextPoint.x + "," + nextPoint.y + " " + target.x + "," + target.y + " ";
            }

            return curveString;
        }
    }

    /**
     * Generates a 'horizontal' curve to replace the horizontal straight segment.
     */
    function generateHorizontalCurve(currentPoint, nextPoint, pPoint, nnPoint, currentDir, nextDir, r) {
        var d = Math.min(Math.abs(nextPoint.y - currentPoint.y), Math.abs(currentPoint.x - pPoint.x), Math.abs(nextPoint.x - nnPoint.x)) / 2;

        if (verticalMode === "curves") {
            if (currentDir !== nextDir) {
                d /= 2;
            }
        } else {
            if (currentDir !== nextDir) {
                d = r;
            }
        }

        var sign = currentDir === "left" ? 1 : -1;
        var x = currentPoint.x + d * sign;
        return " H" + x;

        // var rx = Math.abs((currentPoint.x - pPoint.x) / 2);
        // return "A" + rx + ",20 0 0 1" + x + "," + currentPoint.y + " ";
    }
    
    /**
     * If adjacent rows are too close to have nice individual curves, they will be merged together.
     */
    function smoothBoundary(d) {
        var newBoundary = [];
        var splitIdx = Math.ceil(polygonalBoundary.length / 2);
        
        // Left side: merge too close levels
        var prevX;
        for (var i = 0; i < splitIdx; i+=2) {
            var diff = polygonalBoundary[i].x - prevX;
            if (prevX && Math.abs(diff) < d) {
                // Merge
                newBoundary.pop();
                newBoundary.push({ x: prevX, y: polygonalBoundary[i + 1].y });
            } else {
                if (prevX && diff < 0 && Math.abs(diff) < d) {
                    // Change direction from right to left: push the top one to the left
                    polygonalBoundary[i].x = polygonalBoundary[i + 1].x = prevX - d;
                }
                
                // Normal case
                newBoundary.push(polygonalBoundary[i]);
                newBoundary.push(polygonalBoundary[i + 1]);
                prevX = polygonalBoundary[i].x;
            }
        }
        
        // Right side
        prevX = null;
        for (var i = splitIdx; i < polygonalBoundary.length; i+=2) {
            var diff = polygonalBoundary[i].x - prevX;
            // Merge too close levels
            if (prevX && Math.abs(diff) < d) {
                // Merge
                newBoundary.pop();
                prevX = Math.max(prevX, polygonalBoundary[i].x);
                newBoundary[newBoundary.length - 1].x = prevX;
                newBoundary.push({ x: prevX, y: polygonalBoundary[i + 1].y });
            } else {
                if (prevX && diff < 0 && Math.abs(diff) < d) {
                    // Change direction from right to left: push the top one to the left
                    polygonalBoundary[i].x = polygonalBoundary[i + 1].x = prevX - d;
                }
                
                // Normal case
                newBoundary.push(polygonalBoundary[i]);
                newBoundary.push(polygonalBoundary[i + 1]);
                prevX = polygonalBoundary[i].x;
            }
        }
        
        polygonalBoundary = newBoundary;
    }

    return module;        
};
