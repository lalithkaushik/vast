// Listen to message from extension to execute specific functions
chrome.runtime.onMessage.addListener(function(request) {
	if (request.name === "captureFinding") {
		captureFinding();
	}
});
// End listening

main();

function main() {
	// When the page first loads, retrive all past findings and highlight them in the page.
	loadFindingsToCurrentPage();

	// If automaticCapture mode is ON, add listener to mouseup event.
	chrome.storage.sync.get("automaticCapture", function(item) {
		if (item.automaticCapture) {
			document.body.addEventListener('mouseup', captureFinding);
		}
	});
};

/**
 * Loads existing findings of the current web page and highlight them.
 */
function loadFindingsToCurrentPage() {
	// Selected text stored in local storage with key as web page url
	chrome.storage.sync.get("findings", function(item) {
		if (!item.findings) { return; }

		var webUrl = document.URL.replace(/#.*$/, "").toLowerCase();
		var webHighlights = item.findings[webUrl];
  		if (webHighlights) {
  			for (var i = 0; i < webHighlights.length; i++) {
  				var findingInfo = webHighlights[i];
  				var highlightPath = findingInfo.path.split(/\|/g);
	      		var selection = window.getSelection();
	      		selection.removeAllRanges();
	      		var range = document.createRange();

	      		try {
		      		range.setStart(document.evaluate(highlightPath[0], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue, Number(highlightPath[1]));
		      		range.setEnd(document.evaluate(highlightPath[2], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue, Number(highlightPath[3]));
		      		selection.addRange(range);

		      		highlightFinding(selection, findingInfo, findingInfo.path);
					selection.empty();
	      		} catch (e) {
	      			console.log("Failed to restore previous highlighted text in " + webUrl);
	      			console.log(e);
	      		}
  			}
    	}
	});
};

/**
 * Captures selected text and displays dialog to ask for additional information.
 */
function captureFinding() {
	var selection = window.getSelection();
	if (!selection || selection.type !== "Range") { return; }

	// Highlight it first
	var path = serializeSelection(selection);
	highlightFinding(selection, null, path);

	// Then, save serialized selection to Chrome storage
	queryFindings(document.URL, null, function(finding, webFindings, allFindings) {
		webFindings.push({ "path": path });

		chrome.storage.sync.set({ "findings": allFindings }, function() {
			// And, open a dialog to ask for additional information
			showFindingCaptureDialog(selection, path);
		});
	});
};

/**
 * This async. function queries findings of the given 'webUrl'. The url will be removed hashes and lowered case.
 * If also queries the specific finding based on 'findingXPath'.
 * After retrieving findings, 'callback' will be called with three params: 
 * - specific finding if 'findingXPath' is specified,
 * - findings of 'webUrl', 
 * - and findings of all urls (so that callback can update the entire finding object).
 */
function queryFindings(webUrl, findingXPath, callback) {
	chrome.storage.sync.get("findings", function(item) {
		webUrl = webUrl.replace(/#.*$/, "").toLowerCase(); // Urls with different hashes still in the same page
		if (item.findings) { 
			if (!item.findings[webUrl]) {
				item.findings[webUrl] = []; // Initialize an empty array for the given url as well

				chrome.storage.sync.set(item, function() {
					callback(null, item.findings[webUrl], item.findings);
				});
			}

			var finding = null;
			for (var i = 0; i < item.findings[webUrl].length; i++) {
				if (item.findings[webUrl][i].path === findingXPath) {
					finding = item.findings[webUrl][i];
						
					break;
				}
			}

			callback(finding, item.findings[webUrl], item.findings);
		} else {
			item.findings = {}; // Create an empty object so that it can store next time
			item.findings[webUrl] = []; // Initialize an empty array for the given url as well

			chrome.storage.sync.set(item, function() {
				callback(null, item.findings[webUrl], item.findings);
			});
		}
	});
}

function showFindingCaptureDialog(selection, path) {
	var dialog = $("<div class='sm-capture-dialong' title='Capture Information'></div>").appendTo("body");
	$("<textarea type='text' placeholder='Type a short note about your capture' class='text ui-widget-content ui-corner-all' id='inpNote'></textarea>").appendTo(dialog);
	$("<label for='inpTag' style='padding-top: 10px; padding-bottom: 3px'>Tags (separate by space)</label><input type='text' class='text ui-widget-content ui-corner-all' id='inpTag'></input>").appendTo(dialog);

	dialog.dialog({
		buttons: {
	        "Save": function() {
	        	// Store it together with the finding
	        	queryFindings(document.URL, path, function(finding, webFindings, allFindings) {
					finding.note = dialog.find("#inpNote").val();
					finding.tags = dialog.find("#inpTag").val();

					// Update storage
					chrome.storage.sync.set({ "findings": allFindings });

					// Update interface
					updateHighlight(path, finding);	
	        		
	        		// Tell the extension that a finding is updated
		      		chrome.runtime.sendMessage({ "name": "findingUpdated" });
	        	});
	      		$(this).dialog("close");
	        }, 
	        "Cancel": function() {
	          	$(this).dialog("close");
	        }
      	}, close: function() {
      		selection.empty();
      	}
  	});
}

/**
 * Highlights the finding (selected text). 
 * 'selection' shows its location on the website, and 'findingInfo' stores its note, tags.
 */
function highlightFinding(selection, findingInfo, path) {
	if (!selection) { return; }

	var range = selection.getRangeAt(0);

	// Find lowest common ancestor to start with
	var root = findCommonAncestor(range.startContainer, range.endContainer);
	highlightNode(selection, root, range.startContainer, range.endContainer, findingInfo, path);
}

// http://benpickles.com/articles/51-finding-a-dom-nodes-common-ancestor-using-javascript
function findCommonAncestor(node1, node2) {
	function parents(node) {
  		var nodes = []
  		for (; node; node = node.parentNode) {
    		nodes.unshift(node)
  		}
  		return nodes;
	}

	var parents1 = parents(node1)
	var parents2 = parents(node2)

	if (parents1[0] !== parents2[0]) { 
		return null;
	}

	for (var i = 0; i < parents1.length; i++) {
		if (parents1[i] !== parents2[i]) { 
			return parents1[i - 1]; 
		}
	}

	// Two nodes are the same
	return node1;
};

function highlightNode(selection, currentNode, startNode, endNode, findingInfo, path) {
	if (!currentNode) { return; }

	// DFS: Process itself
	if (selection.containsNode(currentNode, true) && currentNode.nodeName === "#text") {
		var startOffset = currentNode === startNode ? selection.anchorOffset : - 1;
		var endOffset = currentNode === endNode ? selection.focusOffset : - 1;
		highlighTextNode(currentNode, startOffset, endOffset, findingInfo, path);
	}

	if (currentNode === endNode) { return; }

	// Then, go for its children
	for (var i = 0; i < currentNode.childNodes.length; i++) {
		highlightNode(selection, currentNode.childNodes[i], startNode, endNode, findingInfo, path);
	}
};

function highlighTextNode(node, startOffset, endOffset, findingInfo, path) {
	// Ignore white-space text
	var text = node.nodeValue;
	if (text.trim() === "") { return; }

	// Normal text at the beginning
	if (startOffset !== -1) {
		node.parentNode.insertBefore(document.createTextNode(text.substring(0, startOffset)), node);
	}

	// An em node surrounding highlight text
	var emNode = document.createElement("em");
	emNode.classList.add("sm-yellow", "sm-highlight");
	emNode.appendChild(document.createTextNode(text.substring(startOffset, endOffset === -1 ? text.length : endOffset)));
	node.parentNode.insertBefore(emNode, node.nextSibling);

	// Normal text at the end
	if (endOffset !== -1) {
		node.parentNode.insertBefore(document.createTextNode(text.substring(endOffset)), emNode.nextSibling);
	}

	// Remove original text node
	node.parentNode.removeChild(node);

	// Add tooltip to em node
	emNode.setAttribute("id", path); // To easy update later
	updateHighlight(path, findingInfo); // If additional info is available, update the highlight
};

/**
 * Update 'findingInfo' (note, tags) to the highlight defined by 'elementId'.
 */
function updateHighlight(elementId, findingInfo) {
	if (!findingInfo) { return; }
		
	var node = document.getElementById(elementId);
	if (node) {
		var emNodeTitle = findingInfo.note ? findingInfo.note : "";
		emNodeTitle += findingInfo.tags ? findingInfo.tags : "";
		node.setAttribute("title", emNodeTitle);
		if (emNodeTitle.trim() !== "") {
			node.classList.add("sm-note");
		}
	}
}

// http://home.arcor.de/martin.honnen/javascript/storingSelection1.html
// Restore selection based on the path lead to the capture node. Only works with static pages.
// Run it at two different time points may result different paths --> should run only once and pass the value around.
function serializeSelection(selection) {
	function makeXPath (node, currentPath) {
	  /* this should suffice in HTML documents for selectable nodes, XML with namespaces needs more code */
	  currentPath = currentPath || '';
	  switch (node.nodeType) {
	    case 3:
	    case 4:
	      return makeXPath(node.parentNode, 'text()[' + (document.evaluate('preceding-sibling::text()', node, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null).snapshotLength + 1) + ']');
	    case 1:
	      return makeXPath(node.parentNode, node.nodeName + '[' + (document.evaluate('preceding-sibling::' + node.nodeName, node, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null).snapshotLength + 1) + ']' + (currentPath ? '/' + currentPath : ''));
	    case 9:
	      return '/' + currentPath;
	    default:
	      return '';
	  }
	}

	var range = selection.getRangeAt(0);
	if (range) {
	    return makeXPath(range.startContainer) + '|' + range.startOffset + '|' + makeXPath(range.endContainer) + '|' + range.endOffset;
	}

	return "";
};