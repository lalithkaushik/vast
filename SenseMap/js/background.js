document.addEventListener('DOMContentLoaded', function () {
	// Called when the browser action is clicked.
	chrome.browserAction.onClicked.addListener(function() {
		// Get the main view or create if it doesn't exist
		if (!getView(chrome.extension.getURL("history.html"))) {
			// Create it
		    // chrome.windows.create({
		    //     "url": chrome.extension.getURL("history.html"),
		    //     "type": "popup",
		    //     "left": screen.width / 2,
		    //     "width": screen.width / 2,
		    //     "height": screen.height
		    // });
			chrome.tabs.create({
		        "url": chrome.extension.getURL("history.html")
		    });
		}

		// Tell content script to capture selected text
		chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
			chrome.tabs.sendMessage(tabs[0].id, { "name": "captureFinding" });
		});
	});

	function getView(url) {
		var views = chrome.extension.getViews();
		for (var i = 0; i < views.length; i++) {
			var view = views[i];
			if (view.location.href === url) {
				return view;
			}
		}

		return null;
	}
});