<?php 

// You MUST modify app_tokens.php to use your own Oauth tokens
require 'app_tokens.php';

// Create an OAuth connection
require 'tmhOAuth.php';
error_reporting(0);
// Convert tweet text into linkified version
function linkify($text) {
	
	 //Linkify URLs
    $text = ereg_replace("[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]",
    	"<a target='_blank' href=\"\\0\">\\0</a>", $text); 
    	
	// Linkify @mentions
    $text = preg_replace("/\B@(\w+(?!\/))\b/i", 
    	'<a target="_blank" href="https://twitter.com/\\1">@\\1</a>', $text); 
    	
	// Linkify #hashtags
    $text = preg_replace("/\B(?<![=\/])#([\w]+[a-z]+([0-9]+)?)/i", 
    	'<a target="_blank" href="https://twitter.com/#!/search/%23\\1">#\\1</a>', $text); 
    	
    return $text;
}

// Convert a tweet creation date into Twitter format
function twitter_time($time) {

  // Get the number of seconds elapsed since this date
  $delta = time() - strtotime($time);
  if ($delta < 60) {
    return 'less than a minute ago';
  } else if ($delta < 120) {
    return 'about a minute ago';
  } else if ($delta < (60 * 60)) {
    return floor($delta / 60) . ' minutes ago';
  } else if ($delta < (120 * 60)) {
    return 'about an hour ago';
  } else if ($delta < (24 * 60 * 60)) {
    return floor($delta / 3600) . ' hours ago';
  } else if ($delta < (48 * 60 * 60)) {
    return '1 day ago';
  } else {
    return number_format(floor($delta / 86400)) . ' days ago';
  } 
}

		

$connection = new tmhOAuth(array(
  'consumer_key'    => $consumer_key,
  'consumer_secret' => $consumer_secret,
  'user_token'      => $user_token,
  'user_secret'     => $user_secret
));

// Get the timeline with the Twitter API
//$http_code = $connection->request('GET',$connection->url('1.1/statuses/user_timeline.json'), 
//	array( 'screen_name' => 'kenring', 'count' => 10));

		$http_code = $connection->request('GET',$connection->url('1.1/search/tweets.json'), 
		array('q' => '#modi', 'count' => 15));
		
// Request was successful
if ($http_code == 200) {
	
	// Extract the tweets from the API response
	$tweet_data = json_decode($connection->response['response'],true);
	//https://twitter.com/Disaster_Update/status/347257549046808576
	// Accumulate tweets from results
	$tweet_stream = '';	 
	$i=0;
	 
	 var_dump($tweet_data['statuses'][1]);
	/*foreach($tweet_data['statuses'] as $tweet) {
		
		echo $tweet_data['statuses'][$i]['user']['screen_name'];
		// Add this tweet's text to the results
	
		$tweet_stream .= '<div style="display:block;"><a href="http://twitter.com/'.$tweet['statuses'][$i]['user']['screen_name'].'/status/'.$tweet['id_str'].'" target="_blank">'.twitter_time($tweet['created_at']).'</a> <span class="tweet_text">'.linkify($tweet['text']).'</span></div>'.$tweet['point']['coordinates']; $i++;
	}
		
	// Send the tweets back to the Ajax request
	print $tweet_stream;*/
	
// Handle errors from API request
} else {
	if ($http_code == 429) {
		print 'Error: Twitter API rate limit reached';
	} else {
		print 'Error: Twitter was not able to process that request';
	}
}
?>